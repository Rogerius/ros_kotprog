# ROS kötelező programozás házi 

PlatypOUs Mobile Robot Platformra pályakövető program

## Feladat leírása

```
1.1. PlatypOUs pályakövetés

- Basic: Szimulátor élesztése, SLAM tesztelése. ROS node/node-ok implementálása szenzorok adatainak beolvasására és a a robot mozgatására.
- Advanced: ROS rendszer implementálása pályakövetésre szimulált környezetben bármely szenzor felhasználásával(pl. fal mellett haladás adott távolságra LIDAR segítségével).
- Epic: Implementáció és tesztelés a valós hardware-en és/vagy nyűgözz le!
```

## Függvények

### Előre haladás: go_straight(speed, distance, forward)

- speed = (int) sebesség
- distance = (int) haladás távolsága
- forward = (bool) előre, vagy hátra történő haladás
