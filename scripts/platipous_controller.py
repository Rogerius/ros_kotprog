import math
import rospy

from geometry_msgs.msg import Twist, Point, Pose, PoseWithCovariance, Quaternion
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry

class PlatypousController:

    scan = None
    odometry = None

    # Constructor
    def __init__(self):
        rospy.init_node('platypous_controller', anonymous=True)

        self.movement = rospy.Publisher('/cmd_vel/other', Twist, queue_size=10)
        self.scanner = rospy.Subscriber('/scan', LaserScan, self.cb_scan)
        self.wheel = rospy.Subscriber('/odometry/wheel', Odometry, self.cb_odometry)

    def cb_scan(self, msg):
        self.scan = msg

    def cb_odometry(self, msg):
        self.odometry = msg

    def go_straight(self, speed, distance, forward):
        # Implement straght motion here
        # Create and publish msg
        vel_msg = Twist()
        if forward:
            vel_msg.linear.x = speed
        else:
            vel_msg.linear.x = -speed
        vel_msg.linear.y = 0
        vel_msg.linear.z = 0
        vel_msg.angular.x = 0
        vel_msg.angular.y = 0
        vel_msg.angular.z = 0

        # Set loop rate
        rate = rospy.Rate(100) # Hz

        # Publish first msg and note time
        self.movement.publish(vel_msg)
        t0 = rospy.Time.now().to_sec()

        zero_point = self.scan.ranges[360]
        difference = 0
        # Publish msg while the calculated time is up
        while difference < distance and not(rospy.is_shutdown()):
            self.movement.publish(vel_msg)
            difference = abs(self.scan.ranges[360] - zero_point)
            rate.sleep()    # loop rate


        # Set velocity to 0
        vel_msg.linear.x = 0
        self.movement.publish(vel_msg)

    def turn(self, omega, angle, forward):
        # Implement rotation here
        # Create and publish msg
        vel_msg = Twist()
        vel_msg.linear.x = 0
        vel_msg.linear.y = 0
        vel_msg.linear.z = angle
        vel_msg.angular.x = 0
        vel_msg.angular.y = 0
        vel_msg.angular.z = omega

        # Set loop rate
        rate = rospy.Rate(100) # Hz

        # Publish first msg and note time
        self.movement.publish(vel_msg)
        t0 = rospy.Time.now().to_sec()

        zero_point = self.odometry.pose.pose

    # Go to method
    def go_to(self, speed, omega, x, y):
        # Stuff
        pass


if __name__ == '__main__':
    # Init
    pc = PlatypousController()
    # Send turtle on a straight line
    rospy.sleep(1)
    pc.turn(-math.pi/2, 1, True)
    #pc.go_straight(1, 4, True)
